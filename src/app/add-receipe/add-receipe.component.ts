import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-add-receipe',
  templateUrl: './add-receipe.component.html',
  styleUrls: ['./add-receipe.component.scss']
})
export class AddReceipeComponent implements OnInit {
  itemList: { item: 'string', count: 'number' }[];
  itemForm: FormGroup;
  getData = [];
  constructor(private formBuilder: FormBuilder, private service: ServiceService) { }

  ngOnInit() {
    this.itemList = [];
    this.itemForm = this.formBuilder.group({
      itemName: [''],
      itemCount: ['']
    });
  }

  addItem() {
    const formData = this.itemForm.value;
    this.itemList.push(formData);
    this.itemForm.reset();
    this.service.setFormData(formData);
  }

  getItem() {
    this.getData.push(this.service.getFormData());
  }

}
